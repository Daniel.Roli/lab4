package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {
	
	private Integer Cols;
	private Integer Rows;
	private CellState State[][];
	

    public CellGrid(int rows, int columns, CellState initialState) {
		// TODO Auto-generated constructor stub
    	this.Rows = rows;
    	this.Cols = columns;
    	this.State = new CellState[Rows][Cols];
    	for(int i = 0; i < Rows; i++) {
    		for(int j = 0; j < Cols; j++) {
    			State[i][j] = initialState;
    		}
    	}
	}

    @Override
    public int numRows() {
        // TODO Auto-generated method stub
        return Rows;
    }

    @Override
    public int numColumns() {
        // TODO Auto-generated method stub
        return Cols;
    }

    @Override
    public void set(int row, int column, CellState element) {
        // TODO Auto-generated method stub
        State[row][column]=element;
    }

    @Override
    public CellState get(int row, int column) {
        // TODO Auto-generated method stub
        return State[row][column];
    }

    @Override
    public IGrid copy() {
        // TODO Auto-generated method stub
    	Integer row = this.numRows();
    	Integer cols = this.numColumns();
    	CellState Initialstate = CellState.DEAD;
    	CellGrid copy = new CellGrid(row, cols, Initialstate);
    	for(int i = 0; i < Rows; i++) {
    		for(int j = 0; j < Rows; j++) {
    			copy.State[i][j] = this.State[i][j];
    		}
    	}
        return copy;
    }

}
