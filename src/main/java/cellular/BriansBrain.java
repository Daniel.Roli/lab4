package cellular;

import java.util.Random;

import datastructure.CellGrid;
import datastructure.IGrid;

public class BriansBrain implements CellAutomaton{
	
	IGrid currentGeneration;
	
	public BriansBrain(int rows, int columns) {
		currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
		initializeCells();
	}

	@Override
	public CellState getCellState(int row, int col) {
		// TODO
		CellState state = this.currentGeneration.get(row, col);
		return state;
	}

	@Override
	public void initializeCells() {
		Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				if (random.nextBoolean()) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} else {
					currentGeneration.set(row, col, CellState.DEAD);
				}
			}
		}
	}

	@Override
	public void step() {
		IGrid nextGeneration = currentGeneration.copy();
		// TODO
		for(int i=0; i<nextGeneration.numRows();i++) {
			for(int j=0; j<nextGeneration.numColumns(); j++) {
				nextGeneration.set(i, j, getNextCell(i, j));
			}
			
		}
		this.currentGeneration = nextGeneration;
	}

	@Override
	public CellState getNextCell(int row, int col) {
		// TODO Auto-generated method stub
		if(this.currentGeneration.get(row, col).equals(CellState.ALIVE)) {
			return CellState.DYING;
		}else if(this.currentGeneration.get(row, col).equals(CellState.DYING)){
			return CellState.DEAD;
		}else if(this.currentGeneration.get(row, col).equals(CellState.DEAD)) {
			if(countNeighbors(row,col,CellState.ALIVE)==2) {
				return CellState.ALIVE;
			}
		}
		return CellState.DEAD;
	}
	
	private int countNeighbors(int row, int col, CellState state) {
		// TODO
		int count = 0;
		int[][] neighbor = {{-1,-1},{-1,0},{0,-1},{1,0},{0,1},{1,1},{1,-1},{-1,1}};
		for(int i = 0; i<8; i++) {
			try {
				if(this.currentGeneration.get(row+neighbor[i][0], col+neighbor[i][1]).equals(state)) {
					count++;
				}
			}catch(IndexOutOfBoundsException e) {
				
			}
		}
		return count;
	}

	@Override
	public int numberOfRows() {
		// TODO
		int rows = this.currentGeneration.numRows();
		return rows;
	}
	
	

	@Override
	public int numberOfColumns() {
		// TODO
		int cols = this.currentGeneration.numColumns();
		return cols;
	}

	@Override
	public IGrid getGrid() {
		// TODO Auto-generated method stub
		return currentGeneration;
	}

}
